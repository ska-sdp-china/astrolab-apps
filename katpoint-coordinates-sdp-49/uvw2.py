#!/usr/bin/env python3

import os
import sys
import unittest.mock

# RASCIL 0.2b0 imports nifty_gridder but doesn't install it. It's not needed
# for this simulation, so just mock it out.
sys.modules["nifty_gridder"] = unittest.mock.Mock()
# RASCIL gets upset if it can't find its data directory, but doesn't actually
# need it for this simulation. Just give it a fake one.
if "RASCIL2_DATA" not in os.environ:
    os.environ["RASCIL2_DATA"] = "/"

import numpy as np
import katpoint
from katpoint.body import FixedBody
import astropy.units as u
from astropy.coordinates import SkyCoord, Angle
from astropy.time import Time
from rascil2.data_models import Configuration
from rascil2.processing_components import create_visibility
from astropy.coordinates import EarthLocation



freq = 856e6
phase_centre = SkyCoord(ra="3h30m00s", dec="-35d00m00s", frame="icrs")

# Load antennas from a file so that same antennas can be given to katpoint
# and RASCIL.
antennas = []
with open("antennas.txt") as f:
    for line in f:
        if line:
            antennas.append(katpoint.Antenna(line))
antennas = antennas[:2]  # Only need 2 antennas for this demo
print(antennas)
location = antennas[0].ref_position_wgs84
location = EarthLocation(lon= location[1] * u.deg, lat=location[0] * u.deg, height=location[2] * u.meter)
print(location)
config = Configuration(
    location=EarthLocation(location),
    names=[ant.name for ant in antennas],
    mount=np.repeat("azel", len(antennas)),
    xyz=[list(ant.position_enu) for ant in antennas],
    vp_type=np.repeat("unknown", len(antennas)),
    diameter=[ant.diameter.to_value(u.m) for ant in antennas],
    name="MeerKAT",
)

# Create an empty visibility block
bvis = create_visibility(config, [0.0], [freq], phase_centre, channel_bandwidth=[214e6])
rascil_uvw = bvis["uvw"].isel(time=0).sel(baselines=(0, 1)).values
time = Time(bvis["datetime"].data[0])

# Compute UVW coordinates using katpoint
ref_ant = antennas[0].array_reference_antenna()
target = katpoint.Target(FixedBody(phase_centre))
uvws = [np.array(target.uvw(ant, timestamp=time, antenna=ref_ant)) for ant in antennas]
katpoint_uvw = uvws[1] - uvws[0]

print(f"Time: {time}")
print(f"RASCIL:   {rascil_uvw}")
print(f"katpoint:  {katpoint_uvw.xyz.to_value()}")
delta = np.linalg.norm(katpoint_uvw.xyz.to_value() - rascil_uvw)
rel_delta = delta / np.linalg.norm(katpoint_uvw.xyz.to_value())
rel_deg = Angle(rel_delta * u.rad).to_string(u.deg)
print(f"delta:    {delta} (relative: {rel_delta} = {rel_deg})")
